'use strict';

/**
 * An Abacus instance contains the following fields:
 * - rpi (registro de proxima instruccion)
 * - rdm (registro de memoria)
 * - ac (acumulador)
 * - mem (memoria)
 * - terminated (boolean indicating end-of-program has been reached)
 *
 * Of these, only rpi and mem are required for a program to run.
 * The mimimal example of an Abacus machine is
 *     { rpi: 0x300, mem: { 0x300: 0xF000 } }
 * which will immediately terminate when run.
 */

const clone = abacus => {
    return JSON.parse(JSON.stringify(abacus));
};

// prototypal inheritance may save a lot of space
// maybe clone memory on the store operation instead of here (benchmark)
// const clone = abacus => {
//     let after = Object.create(abacus);
//     after.mem = Object.create(abacus.mem);
//     return after;
// }

const operations = {
    directload: (abacus, op) => {
        let after = clone(abacus);
        after.ac = op;
        return after;
    },
    load: (abacus, op) => {
        let after = clone(abacus);
        after.ac = abacus.mem[op];
        return after;
    },
    store: (abacus, op) => {
        let after = clone(abacus);
        after.mem[op] = abacus.ac;
        return after;
    },
    directsum: (abacus, op) => {
        let after = clone(abacus);
        after.ac += op;
        return after;
    },
    indirectsum: (abacus, op) => {
        let after = clone(abacus);
        after.ac += abacus.mem[op];
        return after;
    },
    not: (abacus, op) => {
        let after = clone(abacus);
        after.ac = ~abacus.ac;
        return after;
    },
    ifzero: (abacus, op) => {
        let after = clone(abacus);
        if (abacus.ac === 0)
            after.rpi = op;
        return after;
    },
    ifpos: (abacus, op) => {
        let after = clone(abacus);
        if (abacus.ac > 0)
            after.rpi = op;
        return after;
    },
    ifneg: (abacus, op) => {
        let after = clone(abacus);
        if (abacus.ac < 0)
            after.rpi = op;
        return after;
    },
    terminate: (abacus, op) => {
        let after = clone(abacus);
        after.terminated = true;
        return after;
    },
};

const opcodes = {
    0x0: "directload",
    0x1: "load",
    0x2: "store",
    0x3: "indirectsum",
    0x4: "not",
    0x7: "ifzero",
    0x8: "ifpos",
    0x9: "ifneg",
    0xF: "terminate",
};

const fetch = abacus => {
    if (abacus.terminated)
        throw new Error('Machine is in terminal state');
    let after = clone(abacus);
    after.rdm = abacus.mem[abacus.rpi];
    after.rpi = abacus.rpi + 1;
    return after;
};

const parse = rdm => {
    return {
        co: rdm >> 12,      // codigo de operacion
        op: rdm & 0x0FFF,   // operando
    };
}

const execute = abacus => {
    if (abacus.terminated)
        throw new Error('Machine is in terminal state');
    const {co, op} = parse(abacus.rdm);
    const operation = operations[opcodes[co]];
    return operation(abacus, op);
};

// run a single execution cycle
const cycle = abacus => execute(fetch(abacus));

// cycle until abacus reaches terminal state
const run = abacus => abacus.terminated
    ? abacus
    : run(cycle(abacus));

module.exports = {
    run,
    cycle,
    fetch,
    execute,
};
